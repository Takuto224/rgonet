from torch import nn

class RGOLoss(nn.Module):

    def __init__(self, k):
        super(RGOLoss, self).__init__()
        self.k = k
        self.mse = nn.MSELoss()

    def forward(self, pred_h, gt_h, pred_grav, acc):
        loss = self.mse(pred_h, gt_h) + self.k*self.mse(pred_grav, acc)
        return loss