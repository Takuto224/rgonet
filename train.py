import os
import json
import torch
from torch import optim
import argparse

from data import OxiodDatasetManager, SequenceDataset, get_train_val_list_oxiod
from model import LSTM
from loss import RGOLoss
from utils import save_json
from deep import TrainManager


def main(config, args):

    assert config['dataset'] == 'oxiod'
    DatasetManager = OxiodDatasetManager
    train_path_list, val_path_list = get_train_val_list_oxiod(args.dataset)

    # load dataset
    train_seq_dataset = SequenceDataset(DatasetManager, train_path_list, 
                                        window=config['window'], 
                                        slide=config['window'], 
                                        init_cut=config['init_cut'], 
                                        freq=config['freq'], 
                                        gt=True, 
                                        n_jobs=20)
    val_seq_dataset = SequenceDataset(DatasetManager, val_path_list, 
                                        window=config['window'], 
                                        slide=config['window'], 
                                        init_cut=config['init_cut'],
                                        freq=config['freq'], 
                                        gt=True, 
                                        n_jobs=20)

    batch_train = torch.utils.data.DataLoader(train_seq_dataset, batch_size=1, shuffle=False, num_workers=4)
    batch_val = torch.utils.data.DataLoader(val_seq_dataset, batch_size=1, shuffle=False, num_workers=4)

    # prepare for training
    model = LSTM(input_size=6, output_size=3, hp=config['hp'])
    criterion = RGOLoss(config['k'])
    optimizer = optim.Adam(model.parameters(), lr=config['lr'])

    # train
    tm = TrainManager(model, criterion, optimizer, epochs=config['epochs'], batch_size=config['batch_size'], gpuid=args.gpuid)
    tm.fit(batch_train, batch_val, show_progress=True)

    # save result
    args.result_dir = 'test'
    tm.save(os.path.join(args.result_dir))
    save_json(os.path.join(args.result_dir, 'config.json'), config)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--gpuid', type=int, default=0)
    parser.add_argument('--dataset', type=str)
    parser.add_argument('--result_dir', type=str, default='test')
    args = parser.parse_args()

    config = {
        'dataset': 'oxiod',
        'window': 1600,
        'batch_size': 2048,
        'lr': 1e-3,
        'epochs': 1,
        'init_cut': 0,
        'freq': 100,
        'k': 1,
        'hp': {
            'hidden_size': 64,
            'num_layers': 1,
            'dropout': 0.25,
        }
    }

    main(config, args)
    